# Copyright (C) 2019 Chris Laplante <mostthingsweb@gmail.com>
# Released under the MIT license (see COPYING.MIT for the terms)

IMAGE_FEATURES_append = " package-management"
IMAGE_FEATURES_append = " tools-debug"
IMAGE_FEATURES_append = " ssh-server-dropbear"
IMAGE_FEATURES_append = " bash-completion-pkgs"

IMAGE_INSTALL_append = " \
    vim \
"

inherit core-image
