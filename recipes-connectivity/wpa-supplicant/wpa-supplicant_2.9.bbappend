# A development helper to automatically add wifi credentials to the image.
# These two vars should be set in local.conf.

TEMPERF_WIFI_SSID ?= ""
TEMPERF_WIFI_PSK ?= ""

do_patch[postfuncs] += "add_wifi_config"

python add_wifi_config() {
    ssid = d.getVar("TEMPERF_WIFI_SSID")
    psk = d.getVar("TEMPERF_WIFI_PSK")
    if psk and not ssid:
        bb.fatal("TEMPERF_WIFI_PSK is set, but TEMPERF_WIFI_SSID is not")

    if ssid:
        bb.build.exec_func("add_wifi_config_impl", d)
        psk_note = " [with PSK]" if psk else ""
        msg = d.expand("Added wifi config for SSID: {0}{1}".format(ssid, psk_note))
        bb.verbnote(msg)
}

# Make sure do_unpack is re-run if any of these change
do_unpack[vardeps] += " \
    TEMPERF_WIFI_PSK \
    TEMPERF_WIFI_SSID \
    add_wifi_config \
    add_wifi_config_impl \
"

add_wifi_config_impl() {
    sed -i '/key_mgmt=NONE/c\\tssid="${TEMPERF_WIFI_SSID}"\n\tpsk="${TEMPERF_WIFI_PSK}"' ${WORKDIR}/wpa_supplicant.conf-sane
}

